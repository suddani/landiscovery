#ifndef ILANDISCOVERY_H
#define ILANDISCOVERY_H

#include <string>

#if landiscoveryExport
    #define LAN_DISCOVERY __declspec( dllexport )
#else
    #define LAN_DISCOVERY
#endif

namespace landiscovery
{
class ILanDiscovery
{
    public:
        ILanDiscovery(){}
        virtual ~ILanDiscovery(){}

        virtual bool update(const int& waitMs = 1) = 0;

        virtual const std::string& getMessage(void) const = 0;

        virtual std::string info() = 0;

        virtual std::string getMessage(const std::string& req) const = 0;
    protected:
    private:
};
LAN_DISCOVERY ILanDiscovery* createServer(int broadcast, int host);
LAN_DISCOVERY ILanDiscovery* createServer(const std::string& config);
LAN_DISCOVERY ILanDiscovery* createServer(int broadcast, int host, const std::string& config, bool reload = false);
}

#endif // ILANDISCOVERY_H
