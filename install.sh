./make.sh
if echo $(uname) | grep -q "MINGW"; then
	echo "Can't install on windows"
else 
	sudo cp bin/landiscovery /usr/sbin/landiscovery
	sudo mkdir /etc/landiscovery
	sudo cp install/etc/landiscovery/config /etc/landiscovery/config
	sudo cp install/etc/init.d/landiscovery /etc/init.d/landiscovery
	sudo chmod 755 /etc/init.d/landiscovery
	sudo update-rc.d landiscovery defaults
	sudo service landiscovery start
fi