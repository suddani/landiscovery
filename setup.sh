#create dep folder
mkdir dep

#install tools
echo "Check for wget...."
if ! type -P wget &>/dev/null; then
echo "install wget...."
if echo $(uname) | grep -q "MINGW"; then
	mingw-get install msys-wget
else 
	sudo apt-get install wget
fi
fi

echo "Check for unzip...."
if ! type -P unzip &>/dev/null; then
echo "install unzip...."
if echo $(uname) | grep -q "MINGW"; then
	mingw-get install msys-unzip
else 
	sudo apt-get install unzip
fi
fi

echo "Check for cmake...."
if ! type -P cmake &>/dev/null; then
echo "install cmake...."
if echo $(uname) | grep -q "MINGW"; then
if ! [ -f dep/cmake/bin/cmake.exe ]
then
	cd dep
	wget --no-check-certificate -O cmake.zip http://www.cmake.org/files/v2.8/cmake-2.8.10-win32-x86.zip
	unzip cmake.zip
	rm cmake.zip
	mv cmake-* cmake
	cd ..
fi
else 
	sudo apt-get install cmake
fi
fi

# setup dependecies
cd dep

#libjson
if ! [ -f libjson/libjson.a ]
then
	echo "Installing libjson..."
	rm -r libjson
	wget --no-check-certificate -O libjson_.zip http://sourceforge.net/projects/libjson/files/latest/download?source=files
	unzip libjson_*
	rm libjson_*
	cd libjson
	make
	cd ..
else
	echo "libjson already installed"
fi

#corenet
if [ -f corenet/build/lib/libcorenet.dll.a ] || [ -f corenet/build/lib/libcorenet.a ]
then
	echo "CoreNet already installed"
else
	echo "Installing CoreNet..."
	rm -r corenet
	wget --no-check-certificate -O default.tar.bz2 http://bitbucket.org/suddani/corenet/get/default.tar.bz2
	tar -xvjpf default.tar.bz2
	mv suddani-corenet-* corenet
	rm default.tar.bz2
	
	cd corenet
	mkdir build
	cd build
	if echo $(uname) | grep -q "MINGW"; then
	if type -P cmake &>/dev/null; then
		cmake ./.. -G "MSYS Makefiles"
	else
		../../cmake/bin/cmake ./.. -G "MSYS Makefiles"
	fi
	else
		cmake ./..
	fi
	make
	cd ../..
fi

cd ..

#prepare to run
if echo $(uname) | grep -q "MINGW"; then
	cp dep/corenet/build/lib/libcorenet* bin
fi