#ifndef CSERVERINFO_H
#define CSERVERINFO_H

#include <string>
#include <vector>

class CServerInfo
{
    public:
        CServerInfo(const std::string& name, const std::string& address, const int& port);
        CServerInfo();
        CServerInfo(const CServerInfo& other);
        virtual ~CServerInfo();

        const std::string& getName() const;
        const std::string& getAddress() const;
        const int& getPort() const;

        std::string getName();
        std::string getAddress();
        int getPort();

        std::string getMessage();
        std::string getMessage() const;

        void setName(const std::string& name);
        void setAddress(const std::string& address);
        void setPort(int port);

        //operators
        CServerInfo& operator=(const CServerInfo& other);
        bool operator==(const std::string& other_name);

        static std::vector<CServerInfo> loadServerInfo(const std::string& file);
        static std::vector<CServerInfo> loadServerInfo(const std::string& file, int& host, int& broadcast, int& reload);
    protected:
        std::string name;
        std::string address;
        int port;

        static std::string loadFile(const std::string& file);
    private:
};

#endif // CSERVERINFO_H
