/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include "CLanDiscovery.h"
#include <stdio.h>

#include <iostream>
#include <fstream>

#include <libjson.h>

namespace landiscovery
{
    ILanDiscovery* createServer(int broadcast, int host)
    {
        CLanDiscovery* l = new CLanDiscovery(broadcast);
        l->init(host);
        return l;
    }

    ILanDiscovery* createServer(const std::string& config)
    {
        CLanDiscovery* l = new CLanDiscovery(-1);
        l->init(0, config);
        return l;
    }

    ILanDiscovery* createServer(int broadcast, int host, const std::string& config, bool reload)
    {
        CLanDiscovery* l = new CLanDiscovery(broadcast);
        l->init(host, config, reload);
        return l;
    }
}
CLanDiscovery::CLanDiscovery(const int& broadcast)
{
    coreNet_init();

    assembleWHOMessage(broadcast);

    msg_error = "0.0.0.0:0";
}

void CLanDiscovery::assembleWHOMessage(const int& broadcast)
{
    broadcast_port = broadcast;

    if (broadcast >= 0)
    {
        char msg_buffer[2555];
        sprintf(msg_buffer, "0.0.0.0:%i", broadcast);
        msg = msg_buffer;
    }
    else
    {
        msg = "0.0.0.0:0";
    }
}

CLanDiscovery::~CLanDiscovery()
{
    //dtor
}

bool CLanDiscovery::init(const int& port, const std::string& file, bool reload_every_request)
{
    int host = -1;
    int broadcast = -1;
    int reload = -1;
    infos = CServerInfo::loadServerInfo(file, host, broadcast, reload);

    if (broadcast != -1)
        assembleWHOMessage(broadcast);

    reload_infos = reload == -1 ? reload_every_request : (reload == 1 ? true : false);

    host_port = (host != -1 ? host : port);

    server_socket = coreNet_socket_server(CORENET_SOCKET_UDP, coreNet_address_tmp(CORENET_ANY_HOST, host_port), 0);
    if (server_socket == CORENET_SOCKET_ERROR)
    {
        perror("Create Server Socket");
        return false;
    }
    printf("Create LanDiscovery Socket: %i\n", server_socket);

    selectors = coreNet_selector_create(1);
    selectors[0].Socket = server_socket;
    selectors[0].Read = CORENET_TRUE;

    server_config = file;

    return true;
}

bool CLanDiscovery::update(const int& waitMs)
{
    int ret = coreNet_socket_select(selectors, 1, 0, 1000*waitMs);
    //printf("Select: %i\n", ret);
    if (ret == CORENET_SOCKET_ERROR)
    {
        perror("Socket select");
        return false;
    }
    if (ret > 0 && selectors[0].Read == CORENET_TRUE)
    {
        printf("Received event: %i", ret);
        char buffer[2048];
        CoreNetAddress address;
        int packet_size = coreNet_socket_receive_from(server_socket, buffer, 2048, &address);
        printf(" with size %i", packet_size);
        std::string data(buffer, packet_size);
        //printf("[%s]\n", data.c_str());

        handleRequest(data, &address);

        coreNet_selector_pop(selectors, 1);
    }
    return true;
}

const std::string& CLanDiscovery::getMessage(void) const
{
    return msg;
}

std::string CLanDiscovery::getMessage(const std::string& req) const
{
    for (int i=0;i<infos.size();i++)
    {
        if (req == infos[i].getName())
            return infos[i].getMessage();
    }
    if (req == "WHO")
        return msg;//"0.0.0.0:0";
    return "0.0.0.0:0";
}

std::string CLanDiscovery::info()
{
    char buffer[2048];
    sprintf(buffer, "Start LanDiscovery Server on Port %i and broadcast message: [%s]  -  Reload is %s\n", host_port, getMessage().c_str(), reload_infos ? "active" : "off");
    return std::string(buffer);
}

void CLanDiscovery::handleRequest(const std::string& req, CoreNetAddress* address)
{
    if (reload_infos)
        infos = CServerInfo::loadServerInfo(server_config);

    std::string host_info = getMessage(req);
    //printf("Answer with: %s\n", host_info.c_str());
    coreNet_socket_send_to(server_socket, host_info.c_str(), host_info.length(), address);
}


