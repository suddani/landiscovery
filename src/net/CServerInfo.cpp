/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include "CServerInfo.h"
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <libjson.h>

CServerInfo::CServerInfo(const CServerInfo& other)
{
    (*this) = other;
}

CServerInfo::CServerInfo()
{
}

CServerInfo::CServerInfo(const std::string& name, const std::string& address, const int& port)
{
    this->name = name;
    this->address = address;
    this->port = port;
}

CServerInfo::~CServerInfo()
{
    //dtor
}

CServerInfo& CServerInfo::operator=(const CServerInfo& other)
{
    this->name = other.name;
    this->address = other.address;
    this->port = other.port;
    return *this;
}

bool CServerInfo::operator==(const std::string& other_name)
{
    return name == other_name;
}

const std::string& CServerInfo::getName() const
{
    return name;
}

const std::string& CServerInfo::getAddress() const
{
    return address;
}

const int& CServerInfo::getPort() const
{
    return port;
}

std::string CServerInfo::getName()
{
    return name;
}

std::string CServerInfo::getAddress()
{
    return address;
}

int CServerInfo::getPort()
{
    return port;
}

std::string CServerInfo::getMessage()
{
    char msg_buffer[255];
    sprintf(msg_buffer, "%s:%i", getAddress().c_str(), getPort());
    return std::string(msg_buffer);
}

std::string CServerInfo::getMessage() const
{
    char msg_buffer[255];
    sprintf(msg_buffer, "%s:%i", getAddress().c_str(), getPort());
    return std::string(msg_buffer);
}

void CServerInfo::setName(const std::string& name)
{
    this->name = name;
}

void CServerInfo::setAddress(const std::string& address)
{
    this->address = address;
}

void CServerInfo::setPort(int port)
{
    this->port = port;
}

std::string CServerInfo::loadFile(const std::string& file)
{
    std::ifstream infile;
    infile.open (file.c_str(), std::ifstream::in);
    std::string json;
    while (infile.good())
    {
        char c = infile.get();
        if (infile.good())
            json.push_back(c);
    }
    infile.close();
    return json;
}

void CServerInfo_parseJSON(void* _node, int lvl, std::vector<CServerInfo>& infos, CServerInfo& tmp, int& host, int& broadcast, int& reload)
{
    JSONNODE* node = _node;

    if (node == NULL)
        return;

    if (lvl == 1)
    {
        if (tmp.getName() != "")
            infos.push_back(tmp);
        tmp = CServerInfo("", "", 0);
    }

    JSONNODE_ITERATOR child = json_begin(node);
    while(child != json_end(node))
    {
        if (json_type(*child) == JSON_ARRAY || json_type(*child) == JSON_NODE)
            CServerInfo_parseJSON(*child, lvl+1, infos, tmp, host, broadcast, reload);

        char* node_name = json_name(*child);
        char* node_value = json_as_string(*child);
        int node_value_int = json_as_int(*child);
        //printf("Node: %s = %s, %i\n", node_name, node_value, node_value_int);

        if (lvl == 1 && std::string("name") == node_name)
            tmp.setName(node_value);
        else if (lvl == 1 && std::string("ip") == node_name)
            tmp.setAddress(node_value);
        else if (lvl == 1 && std::string("port") == node_name)
            tmp.setPort(node_value_int);
        else if (lvl == 0 && std::string("host_port") == node_name)
            host = json_as_int(*child);
        else if (lvl == 0 && std::string("broadcast_port") == node_name)
            broadcast = json_as_int(*child);
        else if (lvl == 0 && std::string("reload") == node_name)
            reload = json_as_int(*child);

        json_free(node_name);
        json_free(node_value);

        child++;
    }
    if (lvl == 0)
    {
        if (tmp.getName() != "")
            infos.push_back(tmp);
        tmp = CServerInfo("", "", 0);
    }
}
std::vector<CServerInfo> CServerInfo::loadServerInfo(const std::string& file)
{
    int host;
    int broadcast;
    int reload;
    return loadServerInfo(file, host, broadcast, reload);
}

std::vector<CServerInfo> CServerInfo::loadServerInfo(const std::string& file, int& host, int& broadcast, int& reload)
{
    std::vector<CServerInfo> infos;

    if (file == "")
        return infos;
    //load file
    std::string json = CServerInfo::loadFile(file);

    //parse json
    int ok = json_is_valid(json.c_str());
    if (json_is_valid(json.c_str()) != 1)
        return infos;

    infos.clear();

    CServerInfo tmp;

    JSONNODE* node = json_parse(json.c_str());
    CServerInfo_parseJSON(node, 0, infos, tmp, host, broadcast, reload);
    json_delete(node);

    for (int i=0;i<infos.size();i++)
    {
        printf("ServerInfo: %s, %s, %i\n", infos[i].getName().c_str(), infos[i].getAddress().c_str(), infos[i].getPort());
    }
    return infos;
}

