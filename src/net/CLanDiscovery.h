#ifndef CLANDISCOVERY_H
#define CLANDISCOVERY_H

#include <coreNet/coreNet.hpp>
#include <string>
#include <vector>
#include "CServerInfo.h"

#include <landiscovery/ILanDiscovery.h>

class CLanDiscovery : public landiscovery::ILanDiscovery
{
    public:
        CLanDiscovery(const int& broadcast = -1);
        virtual ~CLanDiscovery();

        bool init(const int& port = 55555, const std::string& file = "", bool reload_every_request = false);

        bool update(const int& waitMs = 1);

        const std::string& getMessage(void) const;

        std::string info();

        std::string getMessage(const std::string& req) const;
    protected:
        coreNetSocket server_socket;
        CoreNetSelector* selectors;

        int host_port;
        int broadcast_port;

        void assembleWHOMessage(const int& broadcast);
        std::string msg;
        std::string msg_error;

        void handleRequest(const std::string& req, CoreNetAddress* address);

        std::vector<CServerInfo> infos;
        std::string server_config;
        bool reload_infos;
    private:
};

#endif // CLANDISCOVERY_H
