/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include "net/CLanDiscovery.h"
#include "net/CServerInfo.h"
#include <stdio.h>

int main(int argc, char* argv[])
{
	if (argc == 2 && std::string("--help") == argv[1])
	{
		printf("Usage:\tlandiscovery [host_port] [port_to_broadcast] [*config_file] [*reload_config_on_request(true/false)]\n");
        printf("or\tlandiscovery [config_file] [*reload_config_on_request(true/false)]\n");
        printf("or\tlandiscovery --help\n");
        printf("\n\nConfig file format:\n");
        printf("{\n\t\"host_port\" : \"55555\" #optional,\n\t\"broadcast_port\" : \"33333\" #optional,\n\t\"reload\" : \"1\" #optional: can be 0 or 1,\n\t\"host\":{\n\t\t\"name\" : \"lol\",\n\t\t\"ip\" : \"192.168.3.7\",\n\t\t\"port\" : 41\n\n\t},\n\t\"host\":{\n\t\t\"name\" : \"second\",\n\t\t\"ip\" : \"192.168.3.56\",\n\t\t\"port\" : 88\n\n\t}\n}");
		return 0;
	}
    if (argc < 2)
    {
        printf("Usage:\tlandiscovery [host_port] [port_to_broadcast] [*config_file] [*reload_config_on_request(true/false)]\n");
        printf("or\tlandiscovery [config_file] [*reload_config_on_request(true/false)]\n");
        printf("or\tlandiscovery --help\n");
        return 1;
    }

    int host;
    int broadcast;

    if (argc > 2 && (std::string("true") != argv[2] || std::string("false") != argv[2]))
    {
        sscanf(argv[1], "%i", &host);
        sscanf(argv[2], "%i", &broadcast);
    }


    //ILanDiscovery server(broadcast);
    //server.init(host, argc > 3 ? argv[3] : (argc >= 2 ? argv[1] : ""), argc > 4 ? (std::string("true") == argv[4]) : (argc == 3 ? std::string("true") == argv[2] : false));

    landiscovery::ILanDiscovery* server = landiscovery::createServer(broadcast, host, argc > 3 ? argv[3] : (argc >= 2 ? argv[1] : ""), argc > 4 ? (std::string("true") == argv[4]) : (argc == 3 ? std::string("true") == argv[2] : false));

    //printf("Start LanDiscovery Server on Port %i and broadcast message: [%s]\n", host, server.getMessage().c_str());
    printf("%s\n", server->info().c_str());

    while(server->update(0)) { printf("tick\n"); }

    return 0;
}
